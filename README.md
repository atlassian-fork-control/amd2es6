# AMD module -> ES6 module transformer for jscodeshift

## Overview

This is a codemod for jscodeshift which converts AMD modules into ES6 modules.

Check this out:

```
define('mymodule', ['jQuery', 'other/module', 'resources/css/stylesheet.css'], ($, otherModule) => {
    return otherModule.Model.extend({
        init() {
            this.title = 'test';
        }
    });
});
```

becomes

```
import $ from "jQuery";
import otherModule from "other/module";
import "resources/css/stylesheet.css";

export default otherModule.Model.extend({
    init() {
        this.title = 'test';
    }
});
```

This codemod also supports the extremely bizzare way of using `exports` as a dependency in order to expose multiple entries from a module:

```
define('mymodule2', ['jQuery', 'exports'], ($, exports) => {
    exports.method1 = (str) => str.length;

    exports.const1 = 42;
});
```

is transformed to become

```
import $ from "jQuery";
import exports from "exports";
export const method1 = (str) => str.length;
export const const1 = 42;
```

## Usage

1. install dependencies: `npm install`
2. run jscodeshift with this transformation and the file you want to transform: `npm start -- FILENAME.js`

