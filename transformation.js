module.exports = function (file, api, options) {
    let j = api.jscodeshift;
    let root = j(file.source);

    root.find(j.CallExpression, { callee: { name: 'define' } })
        .filter(node => node.parentPath.parentPath.name === 'body')
        .forEach(defineElt => {
            const define = defineElt.value;

            const moduleName = define.arguments[0];

            let importStatements = [];
            let exportStatements = [];
            let moduleDefinition = null;

            // dependencies are listed
            if (define.arguments.length > 2) {
                moduleDefinition = define.arguments[2]; // function

                const importedModules = define.arguments[1].elements.map(literal => literal.value); // Array of strings
                const importedAliases = moduleDefinition.params.map(identifier => identifier.name); // list of identifiers

                if (importedAliases.some(alias => alias === 'exports').length === importedModules.some(path => path === 'exports').length) {
                    let oldExportStatements = root.find(j.AssignmentExpression, { operator: '=', left: { type: 'MemberExpression', object: { type: 'Identifier', name: 'exports' } } });
                    let newExportStatements = [];

                    oldExportStatements
                        .filter(node => node.value.left.computed === false)
                        .forEach(node => {
                            j(node).replaceWith(
                                j.exportNamedDeclaration(j.variableDeclaration('const', [ j.variableDeclarator(j.identifier(node.value.left.property.name), node.value.right) ]))
                            );
                        });
                }

                for (let i = 0; i < Math.max(importedModules.length, importedAliases.length); i++) {
                    if (importedAliases.length <= i) {
                        importStatements.push(j.importDeclaration([], j.literal(importedModules[i])));
                    } else {
                        importStatements.push(j.importDeclaration([ j.importDefaultSpecifier(j.identifier(importedAliases[i])) ], j.literal(importedModules[i])));
                    }
                }
            } else {
                moduleDefinition = define.arguments[1]; // module function body
            }

            if (moduleDefinition.body.type === 'BlockStatement' && Array.isArray(moduleDefinition.body.body)) {
                exportStatements = moduleDefinition.body.body;

                exportStatements
                    .filter(node => node.type === 'ReturnStatement')
                    .forEach((returnStatement, idx) => {
                        exportStatements[idx] = j.exportDeclaration(true, returnStatement.argument);
                    });
            }

            j(defineElt.parent).replaceWith(importStatements.concat(exportStatements));
        });

    return root.toSource();
};

